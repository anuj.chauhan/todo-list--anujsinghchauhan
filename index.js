// ........................................... const key=`e887fd8314252650549bc8ee46afd751`;
// .................................... const cardId=`5d848cb62088f237884bd2e7`;
// ..................................   const token=`f7b40548738fbd6a2fa7a91a915b403aa9b317c078834e0e335e899af9e3e9a6`;


window.onload = getChecklist;


function updateItem() {

    Array.from(document.getElementsByClassName("itemInputField")).forEach(function (itemInput) {

        itemInput.addEventListener('keypress', function (event) {

            if (event.keyCode == 13) {

                let itemId = this.parentElement.getAttribute('data-itemid');
                let checklistid = this.parentElement.getAttribute('id');
                let newItemName = this.value;
                url = `https://api.trello.com/1/cards/5d848cb62088f237884bd2e7/checkItem/${itemId}?key=e887fd8314252650549bc8ee46afd751&token=f7b40548738fbd6a2fa7a91a915b403aa9b317c078834e0e335e899af9e3e9a6&name=${newItemName}`

                fetch(url, {
                    method: 'PUT'
                }).then(res => res.json())
                    .then(response => {
                        console.log("item is updated")
                    })
                    .catch(error => console.error(error));
            }
        })
    });
}
function addingEventOnCloseButton() {
    Array.from(document.getElementsByClassName("close")).forEach(function (item) {

        item.addEventListener('click', function (event) {
            console.log(event);
            let itemId = this.parentElement.getAttribute('data-itemid');
            let checklistid = this.parentElement.getAttribute('id');
            console.log(checklistid);
            url = `https://api.trello.com/1/checklists/${checklistid}/checkItems/${itemId}?key=e887fd8314252650549bc8ee46afd751&token=f7b40548738fbd6a2fa7a91a915b403aa9b317c078834e0e335e899af9e3e9a6`
            deleteItem(url);
            this.parentElement.remove();
        })
    });
}
function checkedItem() {

    Array.from(document.getElementsByClassName("checkbox")).forEach(function (checkboxButton) {

        checkboxButton.addEventListener('click', function (event) {
            let itemId = this.parentElement.getAttribute('data-itemid');
            
            if (this.checked == true) {

                
                url = `https://api.trello.com/1/cards/5d848cb62088f237884bd2e7/checkItem/${itemId}?key=e887fd8314252650549bc8ee46afd751&token=f7b40548738fbd6a2fa7a91a915b403aa9b317c078834e0e335e899af9e3e9a6&state=complete`

                fetch(url, {
                    method: 'PUT'
                }).then(res => res.json())
                    .then(response => {
                        console.log("checkbox is checked")
                        this.nextElementSibling.style.textDecoration = "line-through";
                    })
                    .catch(error => console.error(error));
            }
            else
            {
                url = `https://api.trello.com/1/cards/5d848cb62088f237884bd2e7/checkItem/${itemId}?key=e887fd8314252650549bc8ee46afd751&token=f7b40548738fbd6a2fa7a91a915b403aa9b317c078834e0e335e899af9e3e9a6&state=incomplete`

                fetch(url, {
                    method: 'PUT'
                }).then(res => res.json())
                    .then(response => {
                        console.log("checkbox is unchecked")
                        this.nextElementSibling.style.textDecoration = "none";
                    })
                    .catch(error => console.error(error));



            }

        })
    });
}
function getItem(elementid) {

    fetch(`https://api.trello.com/1/checklists/${elementid}/checkItems?key=e887fd8314252650549bc8ee46afd751&token=f7b40548738fbd6a2fa7a91a915b403aa9b317c078834e0e335e899af9e3e9a6`)
        .then(function (response) {
            return response.json();
        })
        .then(function (myJson) {
            myJson.forEach(element => {
                let newItemNode = document.createElement("div");
                newItemNode.className = "item";
                newItemNode.setAttribute('data-itemId', `${element.id}`);
                newItemNode.setAttribute('id', `${elementid}`);
                let checkNode = document.createElement("input");
                checkNode.setAttribute("type", "checkbox");
                checkNode.className = "checkbox";
                let inputfield = document.createElement("input");
                inputfield.setAttribute("type", "text");
                inputfield.className = "itemInputField";
                inputfield.value = element.name;
                let closenode = document.createElement("button");
                closenode.setAttribute("type", "button");
                closenode.className = "close";
                closenode.textContent = "X";
                if (element.state === "complete") {
                    checkNode.checked = true;
                    inputfield.style.textDecoration = "line-through";
                }
                newItemNode.append(checkNode);
                newItemNode.append(inputfield);
                newItemNode.append(closenode);
                document.getElementById(elementid).append(newItemNode);
                console.log(document.getElementById(elementid));
            });
            console.log(`checklist item is fetched for ${elementid}`)
            addingEventOnCloseButton();
            checkedItem();
            updateItem();
        });
}

function getChecklist() {
    fetch('https://api.trello.com/1/cards/5d848cb62088f237884bd2e7/checklists?checkItems=all&checkItem_fields=name%2CnameData%2Cpos%2Cstate&filter=all&fields=all&key=e887fd8314252650549bc8ee46afd751&token=f7b40548738fbd6a2fa7a91a915b403aa9b317c078834e0e335e899af9e3e9a6')
        .then(function (response) {
            return response.json();
        })
        .then(function (myJson) {
            myJson.forEach(element => {
                let newnode = document.createElement("div")
                newnode.className = "checklist";
                newnode.setAttribute('data-checklistIdName', `${element.id}`);
                newnode.setAttribute('id', `${element.id}`)
                newnode.textContent = element.name;
                document.querySelector(".checklistDetail").append(newnode);
                let deletNode = document.createElement("button");
                deletNode.className = "btn-delete";
                deletNode.setAttribute("type", "button");
                deletNode.textContent = "DELETE";
                newnode.append(deletNode);

                let addItembutton = document.createElement("button");
                let p = document.createElement("p");

                addItembutton.className = "btn-item";
                addItembutton.setAttribute("type", "button");
                addItembutton.textContent = "ADD ITEM";
                p.append(addItembutton);
                newnode.append(p);
                let input = document.createElement("input");
                input.setAttribute("type", "text");
                input.className = "item-input";
                input.setAttribute("placeHolder", "enter the item");
                ///////////////////////
                getItem(element.id);
                ///////////////////////////

                newnode.append(input);
                addItembutton.addEventListener("click", function (event) {

                    let itemName = input.value;

                    // let id = addItembutton.getAttribute("id")
                    url = `https://api.trello.com/1/checklists/${element.id}/checkItems?name=${itemName}&pos=bottom&checked=false&key=e887fd8314252650549bc8ee46afd751&token=f7b40548738fbd6a2fa7a91a915b403aa9b317c078834e0e335e899af9e3e9a6`
                    fetch(url, {
                        method: 'POST'
                    }).then(res => res.json())
                        .then(itemid => {

                            let newItemNode = document.createElement("div");
                            newItemNode.className = "item";
                            newItemNode.setAttribute('data-itemId', `${itemid.id}`);
                            newItemNode.setAttribute('id', `${element.id}`)
                            let checkNode = document.createElement("input");
                            checkNode.setAttribute("type", "checkbox");
                            checkNode.className = "checkbox";
                            let inputfield = document.createElement("input");
                            inputfield.className = "itemInputField";
                            inputfield.setAttribute("type", "text");
                            inputfield.value = itemid.name;
                            let closenode = document.createElement("button");
                            closenode.setAttribute("type", "button");
                            closenode.className = "close";
                            closenode.textContent = "X";

                            newItemNode.append(checkNode);

                            newItemNode.append(inputfield);
                            newItemNode.append(closenode);
                            document.getElementById(element.id).append(newItemNode);
                            input.value = ""
                            addingEventOnCloseButton();
                            checkedItem();
                            updateItem();
                        })
                        .catch(error => console.error('Error:', error));
                })
            });
            Array.from(document.getElementsByClassName("btn-delete")).forEach(function (elem) {
                console.log('1');
                elem.addEventListener('click', function (event) {
                    console.log(event);

                    let checklistid = this.parentElement.getAttribute('data-checklistIdName');
                    console.log(checklistid);
                    url = `https://api.trello.com/1/cards/5d848cb62088f237884bd2e7/checklists/${checklistid}?key=e887fd8314252650549bc8ee46afd751&token=f7b40548738fbd6a2fa7a91a915b403aa9b317c078834e0e335e899af9e3e9a6`;
                    deleteChecklist(url);
                    this.parentElement.remove();
                })
            });
            let x = document.getElementsByClassName("close");

            console.log(Array.from(x));
        });
}

// ..........................................deleteChecklistButton......................................................
function deleteChecklist(url) {
    fetch(url, {
        method: 'DELETE'
    }).then(res => res.json())
        .then(response => console.log("checklist deleted"))
        .catch(error => console.error(error));
}

// ...................................................deleteItemButtton.............................................

function deleteItem(url) {
    fetch(url, {
        method: 'DELETE'
    }).then(res => {
        console.log("item dle")
        res.json()
    }).catch(error => console.error(error));
}
// ...........................................adding the checklist...................................................


function addchecklist(url, checklistName) {
    fetch(url, {
        method: 'POST'
    }).then(res => res.json())
        .then(response => {
            console.log('Success:');
            let newnode = document.createElement("div")
            newnode.className = "checklist";
            newnode.setAttribute('data-checklistIdName', `${response.id}`);
            newnode.setAttribute('id', `${response.id}`)
            newnode.textContent = checklistName;
            document.querySelector(".checklistDetail").append(newnode);
            let deletNode = document.createElement("button");
            deletNode.className = "btn-delete";
            deletNode.setAttribute("type", "button");
            deletNode.textContent = "DELETE";
            newnode.append(deletNode);
            let addItembutton = document.createElement("button");
            let p = document.createElement("p");
            addItembutton.className = "btn-item";
            addItembutton.setAttribute("type", "button");
            addItembutton.textContent = "ADD ITEM";
            p.append(addItembutton);
            newnode.append(addItembutton);
            let input = document.createElement("input");
            input.setAttribute("type", "text");
            input.className = "item-input";
            input.setAttribute("placeHolder", "enter the item");
            newnode.append(input);
            deletNode.addEventListener('click', function (event) {
                event.preventDefault();
                let checklistid = this.parentElement.getAttribute('data-checklistIdName');
                console.log(checklistid);
                url = `https://api.trello.com/1/cards/5d848cb62088f237884bd2e7/checklists/${checklistid}?key=e887fd8314252650549bc8ee46afd751&token=f7b40548738fbd6a2fa7a91a915b403aa9b317c078834e0e335e899af9e3e9a6`;
                deleteChecklist(url);
                this.parentElement.remove();
            })

            // ........................................adding Event on add .addingEventOnCloseButton..........................



            addItembutton.addEventListener("click", function (event) {
                let itemName = input.value;
                url = `https://api.trello.com/1/checklists/${response.id}/checkItems?name=${itemName}&pos=bottom&checked=false&key=e887fd8314252650549bc8ee46afd751&token=f7b40548738fbd6a2fa7a91a915b403aa9b317c078834e0e335e899af9e3e9a6`
                fetch(url, {
                    method: 'POST'
                })
                    .then(res => res.json())
                    .then(itemid => {
                        let newItemNode = document.createElement("div");
                        newItemNode.className = "item";
                        newItemNode.setAttribute('data-itemId', `${itemid.id}`);
                        newItemNode.setAttribute('id', `${response.id}`);
                        let checkNode = document.createElement("input");
                        checkNode.setAttribute("type", "checkbox");
                        checkNode.className = "checkbox";
                        let inputfield = document.createElement("input");
                        inputfield.setAttribute("type", "text");
                        inputfield.className = "itemInputField";
                        inputfield.value = itemid.name;
                        let closenode = document.createElement("button");
                        closenode.setAttribute("type", "button");
                        closenode.className = "close";
                        closenode.textContent = "X";
                        newItemNode.append(checkNode);
                        newItemNode.append(inputfield);
                        newItemNode.append(closenode);
                        document.getElementById(response.id).append(newItemNode);
                        checkedItem();
                        input.value = ""
                        closenode.addEventListener('click', function (event) {
                            console.log(event);
                            let itemId = this.parentElement.getAttribute('data-itemid');
                            let checklistid = this.parentElement.getAttribute('id');
                            console.log(checklistid);
                            url = `https://api.trello.com/1/checklists/${checklistid}/checkItems/${itemId}?key=e887fd8314252650549bc8ee46afd751&token=f7b40548738fbd6a2fa7a91a915b403aa9b317c078834e0e335e899af9e3e9a6`
                           
                            deleteItem(url);
                            this.parentElement.remove();
                        })
                    })
                    .catch(error => console.error('Error:', error));
            }).catch(error => console.error('Error:', error));
        })

}

// .........................................................adding checklist.. checklist.................................



addingEventOnCloseButton();
checkedItem();
updateItem();


document.querySelector(".add-checklist").addEventListener("click", function (e) {
    e.preventDefault();
    let checklistName = document.querySelector("input").value;
    if (checklistName.length !== 0) {

        console.log(checklistName);
        let url = `https://api.trello.com/1/cards/5d848cb62088f237884bd2e7/checklists?key=e887fd8314252650549bc8ee46afd751&token=f7b40548738fbd6a2fa7a91a915b403aa9b317c078834e0e335e899af9e3e9a6&name=${checklistName}`;
        document.querySelector("input").value = " ";
        addchecklist(url, checklistName);
    }
    else {
        alert("Enter checklist name");
    }
})








